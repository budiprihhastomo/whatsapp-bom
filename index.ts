import { WAClient, Presence, MessageType } from "@adiwajshing/baileys";
import * as fs from "fs";
import moment from "moment";

const client = new WAClient();

const connectToWhatsApp = async () => {
  const credentialFile: string = "auth_info";
  try {
    // Check Apakah Sudah Punya Credential File
    const isExistCredentialFile = fs.existsSync(`./${credentialFile}.json`)
      ? require(`./${credentialFile}.json`)
      : null;
    const { id, name } = await client.connectSlim(isExistCredentialFile);

    // Buat Credential File Jika Pertama Kali Login
    if (!isExistCredentialFile) {
      const credential = client.base64EncodedAuthInfo();
      fs.writeFileSync(
        `./${credentialFile}.json`,
        JSON.stringify(credential, null, "\t")
      );
    }

    // Output
    console.log(`My ID : ${id}`);
    console.log(`My Name : ${name}`);
  } catch {
    console.error("Kesalahan Menghubungkan Ke WhatsApp");
  }
};

// Mendapatkan Semua Kontak
const getAllContacts = async () => {
  const [_, contacts] = await client.receiveChatsAndContacts();
  return contacts.reduce<Array<object>>((a, { jid, name, notify }) => {
    if ((!jid.includes("@g.us") && name) || notify)
      a = [...a, { jid, name: name || notify }];
    return a;
  }, []);
};

const getStatusSomeone = (contacts: object[]) => {
  client.setOnPresenceUpdate(({ id, type }) => {
    const findDetailPerson = contacts.find((val: any) => val.jid === id);
    console.log({
      ...findDetailPerson,
      status: type === "available" ? "Online" : "Offline",
      waktu: moment().format("d-MMM-YYYY hh:mm:ss"),
    });
  });
  contacts.map(async (contact: any) => {
    await client.requestPresenceUpdate(contact.jid);
  });
};

const bombChat = (number: string) => {
  client
    .sendMessage(
      number,
      "سُبْحَانَ اللهِ وَبِحَمْدِهِ أَسْتَغْفِرُ اللهَ وَأَتُوبُ إِلَيْهِ",
      MessageType.text
    )
    .then(() => {
      console.log("Send");
    });
};

(async () => {
  await connectToWhatsApp();
  // const contacts = await getAllContacts();
  // getStatusSomeone(contacts);
  const delay = () => new Promise((res) => setTimeout(() => res(), 1000));
  for (let i = 0; i <= 500; i++) {
    bombChat("628980887258@s.whatsapp.net");
    await delay();
  }
})();
