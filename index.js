"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var baileys_1 = require("@adiwajshing/baileys");
var fs = __importStar(require("fs"));
var moment_1 = __importDefault(require("moment"));
var client = new baileys_1.WAClient();
var connectToWhatsApp = function () { return __awaiter(void 0, void 0, void 0, function () {
    var credentialFile, isExistCredentialFile, _a, id, name_1, credential, _b;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                credentialFile = "auth_info";
                _c.label = 1;
            case 1:
                _c.trys.push([1, 3, , 4]);
                isExistCredentialFile = fs.existsSync("./" + credentialFile + ".json")
                    ? require("./" + credentialFile + ".json")
                    : null;
                return [4 /*yield*/, client.connectSlim(isExistCredentialFile)];
            case 2:
                _a = _c.sent(), id = _a.id, name_1 = _a.name;
                // Buat Credential File Jika Pertama Kali Login
                if (!isExistCredentialFile) {
                    credential = client.base64EncodedAuthInfo();
                    fs.writeFileSync("./" + credentialFile + ".json", JSON.stringify(credential, null, "\t"));
                }
                // Output
                console.log("My ID : " + id);
                console.log("My Name : " + name_1);
                return [3 /*break*/, 4];
            case 3:
                _b = _c.sent();
                console.error("Kesalahan Menghubungkan Ke WhatsApp");
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
// Mendapatkan Semua Kontak
var getAllContacts = function () { return __awaiter(void 0, void 0, void 0, function () {
    var _a, _, contacts;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0: return [4 /*yield*/, client.receiveChatsAndContacts()];
            case 1:
                _a = _b.sent(), _ = _a[0], contacts = _a[1];
                return [2 /*return*/, contacts.reduce(function (a, _a) {
                        var jid = _a.jid, name = _a.name, notify = _a.notify;
                        if ((!jid.includes("@g.us") && name) || notify)
                            a = __spreadArrays(a, [{ jid: jid, name: name || notify }]);
                        return a;
                    }, [])];
        }
    });
}); };
var getStatusSomeone = function (contacts) {
    client.setOnPresenceUpdate(function (_a) {
        var id = _a.id, type = _a.type;
        var findDetailPerson = contacts.find(function (val) { return val.jid === id; });
        console.log(__assign(__assign({}, findDetailPerson), { status: type === "available" ? "Online" : "Offline", waktu: moment_1.default().format("d-MMM-YYYY hh:mm:ss") }));
    });
    contacts.map(function (contact) { return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, client.requestPresenceUpdate(contact.jid)];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
};
var bombChat = function (number) {
    client
        .sendMessage(number, "سُبْحَانَ اللهِ وَبِحَمْدِهِ أَسْتَغْفِرُ اللهَ وَأَتُوبُ إِلَيْهِ", baileys_1.MessageType.text)
        .then(function () {
        console.log("Send");
    });
};
(function () { return __awaiter(void 0, void 0, void 0, function () {
    var delay, i;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, connectToWhatsApp()];
            case 1:
                _a.sent();
                delay = function () { return new Promise(function (res) { return setTimeout(function () { return res(); }, 1000); }); };
                i = 0;
                _a.label = 2;
            case 2:
                if (!(i <= 500)) return [3 /*break*/, 5];
                bombChat("628980887258@s.whatsapp.net");
                return [4 /*yield*/, delay()];
            case 3:
                _a.sent();
                _a.label = 4;
            case 4:
                i++;
                return [3 /*break*/, 2];
            case 5: return [2 /*return*/];
        }
    });
}); })();
